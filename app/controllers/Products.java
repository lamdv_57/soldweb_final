package controllers;

import com.avaje.ebean.Ebean;
import models.Product;
import models.Tag;
import play.data.Form;
import play.mvc.Result;
import play.mvc.Controller;
import scala.reflect.internal.ReificationSupport;
import views.html.products.list;
import views.html.products.details;
import views.html.products.productDetail;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Do Lam on 28/03/2015.
 */
public class Products extends Controller {

    public static Result list(){
        List<Product> products = Product.findAll();
        return ok(list.render(products));
    }

    private static final Form<Product> productForm = Form.form(Product.class);
    public static Result newProduct(){
        return ok(details.render(productForm));
    }

    public static Result details(Product product){
        if (product == null){
            return notFound(String.format("Product %s not exist", product.ean));
        }
        return ok(productDetail.render(product));
    }

    public static Result save(){
        Form<Product> boundForm = productForm.bindFromRequest();
        if (boundForm.hasErrors()){
            flash("error", "Please correct thr form below");
            return badRequest(details.render(boundForm));
        }
        Product product = boundForm.get();
        List<Tag> tags = new ArrayList<Tag>();
        for (Tag tag : product.tags){
            if (tag.id != null){
                tags.add(Tag.findById(tag.id));
            }
        }
        product.tags = tags;
        if (product.id == null){
            Ebean.save(product);
        }else {
            if (product.ean != null){
                flash("Error, Ean exits !!!", "Please enter again Ean");
            }
            Ebean.update(product);
        }
        return redirect(routes.Products.list());
    }

    public static Result delete(String ean) {
        Product product = Product.findByEan(ean);
        if(product == null) {
            return notFound(String.format("Product %s does not exists.", ean));
        }
        for (Tag tag: product.tags) {
            tag.products.remove(product);
            tag.save();
        }

        product.delete();
        return redirect(routes.Products.list());
    }

    public static Result edit(Product product){
        if (product == null){
            return notFound(String.format("Product %s not exist", product.ean));
        }
        Form<Product> filledForm = productForm.fill(product);
        return ok(details.render(filledForm));
    }

}
