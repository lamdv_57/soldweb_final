package controllers;

import models.Tag;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.products.listTag;
import views.html.products.tagDetali;

import java.util.List;

/**
 * Created by Do Lam on 04/04/2015.
 */
public class Tags extends Controller {
    public static Result listTags(){
        List<Tag> tags = Tag.findAll();
        return ok(listTag.render(tags));
    }

    public static Result details(Tag tag){
        return ok(tagDetali.render(tag.products));
    }
}
