package models;

import com.avaje.ebean.Ebean;
import play.mvc.PathBindable;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Do Lam on 29/03/2015.
 */
@Entity
public class Tag extends Model implements PathBindable<Tag> {
    @Id
    public Long id;
    @Constraints.Required
    public String name;

    @ManyToMany(mappedBy="tags")
    public List<Product> products;

    public Tag(){}

    public Tag(Long id, String name, Collection<Product> products){
        this.id = id;
        this.name = name;
        this.products = new LinkedList<Product>(products);
        for (Product product : products){
            product.tags.add(this);
        }
    }

    public static Finder<Long, Tag> find =
            new Finder<>(Long.class, Tag.class);

    public static Tag findById(Long id) {
        return find.byId(id);
    }

    public static List<Tag> findAll() {
        return find.all();
    }
    public static Tag findByName(String name) {
        return find.where().eq("name",name).findUnique();
    }

    @Override
    public Tag bind(String key, String value) {
        return findByName(value);
    }

    @Override
    public String unbind(String s) {
        return name;
    }

    @Override
    public String javascriptUnbind() {
        return name;
    }
}
