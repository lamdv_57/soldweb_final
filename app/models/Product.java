package models;

import play.db.ebean.Model;
import play.mvc.PathBindable;
import play.data.validation.Constraints;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Do Lam on 28/03/2015.
 */
@Entity
public class Product extends Model implements PathBindable<Product> {
    @Id
    public Long id;
    @Constraints.Required
    public String ean;
    @Constraints.Required
    public String name;
    public String description;
    public double price;
    public int remainProduct;
    public String image;

    @ManyToMany
    public List<Tag> tags;

    public static Finder<Long,Product> find =
            new Finder<Long,Product>(Long.class, Product.class);

    public Product() {}
    public Product(String ean, String name, String description, double price, int remainProduct) {
        this.ean = ean;
        this.name = name;
        this.description = description;
        this.price = price;
        this.remainProduct = remainProduct;
    }
    public String toString() {
        return String.format("%s - %s", ean, name);
    }

    public static List<Product> findAll() {
        return find.all();
    }

    public static Product findByEan(String ean) {
        return find.where().eq("ean", ean).findUnique();
    }

    public static List<Product> findByName(String term) {
        return find.where().eq("name", term).findList();
    }

    @Override
    public Product bind(String key, String value) {
        return findByEan(value);
    }

    @Override
    public String unbind(String key) {
        return ean;
    }

    @Override
    public String javascriptUnbind() {
        return ean;
    }


}
